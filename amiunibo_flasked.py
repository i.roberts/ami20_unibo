import pickle

from flask import Flask, request
from flask_json import FlaskJSON, JsonError, as_json

import tensorflow as tf
import torch
import pandas as pd
import numpy as np
from tqdm.notebook import tqdm
from transformers import AutoTokenizer, AutoModel
from transformers import BertTokenizer, BertForSequenceClassification
from torch.utils.data import TensorDataset
from sklearn.metrics import f1_score
from torch.utils.data import DataLoader

app = Flask(__name__)
# Don't add an extra "status" property to JSON responses - this would break the API contract
app.config['JSON_ADD_STATUS'] = False
# Don't sort properties alphabetically in response JSON
app.config['JSON_SORT_KEYS'] = False

json_app = FlaskJSON(app)


# Returns the F1 score computed on the predictions
def f1_score_func(preds, labels):
    preds_flat=np.argmax(preds, axis=1).flatten()
    labels_flat=labels.flatten()
    return f1_score(labels_flat, preds_flat, average='weighted')

# Prints the accuracy of the model for each class
def accuracy_per_class(preds, labels):
    label_dict_inverse = {v: k for k, v in label_dict.items()}

    preds_flat = np.argmax(preds, axis=1).flatten()
    labels_flat = labels.flatten()

    for label in np.unique(labels_flat):
        y_preds = preds_flat[labels_flat==label]
        y_true = labels_flat[labels_flat==label]
        print(f'Class: {label_dict_inverse[label]}')
        print(f'Accuracy:{len(y_preds[y_preds==label])}/{len(y_true)}\n')

# Evaluates the model using the test set
def predict(dataset_test, device, model):
    predictions = []

    for row in dataset_test:
      row = tuple(r.to(device) for r in row)
      inputs = {'input_ids': row[0],
        'attention_mask': row[1]
        }

      with torch.no_grad():
          outputs = model(**inputs)

      logits = outputs[0]
      logits = logits.detach().cpu().numpy()
      predictions.append(logits)

    return predictions

@json_app.invalid_json_error
def invalid_request_error(e):
    """Generates a valid ELG "failure" response if the request cannot be parsed"""
    raise JsonError(status_=400, failure={'errors': [
        {'code': 'elg.request.invalid', 'text': 'Invalid request message'}
    ]})


@app.route('/process', methods=['POST'])
@as_json
def process_request():
    """Main request processing logic - accepts a JSON request and returns a JSON response."""
    data = request.get_json()
    # sanity checks on the request message
    if (data.get('type') != 'text') or ('content' not in data):
        invalid_request_error(None)

    content = data['content']
    result = do_nlp(content)
    return dict(response={'type': 'annotations', 'annotations': result})


@app.before_first_request
def load_models():
    global device
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    global model
    model = torch.load('data/3classmodel3', map_location=torch.device(device))
    global tokenizer
    tokenizer = AutoTokenizer.from_pretrained("m-polignano-uniba/bert_uncased_L-12_H-768_A-12_italian_alb3rt0", do_lower_case=True)


def do_nlp(content):
    #implementation for a single sentence
    sentence_anns = []
    encoded_data_test = tokenizer.batch_encode_plus(
        [content],
        add_special_tokens=True,
        return_attention_mask=True,
        pad_to_max_length=True,
        max_length=256,
        return_tensors='pt',
        truncation=True
    )

    # Extract IDs, attention masks and labels from test dataset
    input_ids_test = encoded_data_test['input_ids']
    attention_masks_test = encoded_data_test['attention_mask']

    dataset_test = TensorDataset(input_ids_test, attention_masks_test)

    dataloader_test = DataLoader(dataset_test)

    # Predict values for test dataset
    predictions = predict(dataloader_test, device, model)

    results = []
    for i, prediction in enumerate(predictions):
      predicted = np.argmax(prediction, axis=1)[0]
      # print(f"index: {i} -- prediction: {predicted}")
      results.append(predicted)

    # jsonify the result
    for sent, label in zip([content], results):
        sentence_anns.append({'start': 0, 'end': len(sent), 'features': {'label': int(label)}})
    return {'Sentence': sentence_anns}


if __name__ == '__main__':
    app.run()
